import QuickPaper from 'quick-paper';

// 兼容文件
import '@hai2007/polyfill/Promise.js';

// 引入启动界面
import App from './App.paper';

// 引入基础样式
import '@hai2007/style/normalize.css';

// 引入路由
import routers from './router.js'; QuickPaper.use(routers);

// 设置标题方法
let _title = document.title;
QuickPaper.prototype.$setTitle = msg => {
    document.title = msg + " " + _title;
};

//根对象
window.quickPaper = new QuickPaper({

    //挂载点
    el: document.getElementById('root'),

    // 启动
    render: createElement => createElement(App)
});
